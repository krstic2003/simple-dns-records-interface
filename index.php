<?php

require 'config.php';

$path = '/v1/user/self/zone/'.$configs["domain"].'/record';

require 'connector.php';

?>

<!DOCTYPE html>
<html>
<head>
	<title>Simple Interface for DNS records</title>
	<link rel="stylesheet" type="text/css" href="css/style.css" />
	<script type="text/javascript" src="js/custom.js"></script>
</head>
<body>
	<main id="main">
		<h2>Add a record</h2>
		<form action="add-record.php" method="POST" id="add-record" onchange="showHide()">
			<select name="type" id="record-type">
				<option value="A">A</option>
				<option value="AAAA">AAAA</option>
				<option value="MX">MX</option>
				<option value="ANAME">ANAME</option>
				<option value="CNAME">CNAME</option>
				<option value="NS">NS</option>
				<option value="TXT">TXT</option>
				<option value="SRV">SRV</option>
			</select>
			<input type="text" name="name" id="name" placeholder="Name">
			<input type="text" name="content" id="content" placeholder="Content">
			<input type="number" name="prio" id="prio" placeholder="Priority">
			<input type="number" name="port" id="port" placeholder="Port">
			<input type="text" name="weight" id="weight" placeholder="Weight">
			<input type="number" name="ttl" id="ttl" placeholder="TTL">
			<input type="text" name="note" id="note" placeholder="Note (optional)">
			<input type="submit" value="Submit">
		</form>	

		<h2>DNS Records for <?php echo $configs["domain"]; ?></h2>
		<table class="items-list" cellspacing="0">
			<tr>
				<th>Type</th>
				<th>ID</th>
				<th>Name</th>
				<th>Content</th>
				<th>Prio</th>
				<th>Port</th>
				<th>Weight</th>
				<th>TTL</th>
				<th>Note</th>
				<th>Actions</th>
			</tr>
		<?php
		foreach ($response->items as $v) {
			echo "<tr><td>".$v->type."</td><td>".$v->id."</td><td>".$v->name."</td><td>".$v->content."</td><td>".$v->prio."</td><td>".$v->port."</td><td>".$v->weight."</td><td>".$v->ttl."</td><td>".$v->note."</td><td><form method='GET' action='delete-record.php'><input type='hidden' name='del_id' value='".$v->id."'><input type='submit' value='Delete'></form></td></tr>";
		}

		?>
		</table>

	
	</main>

</body>
</html>