<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require 'config.php';

$id = $_GET['del_id'];

$time = time();
$method = 'DELETE';
$path = '/v1/user/self/zone/'.$configs["domain"].'/record/'.$id;
$api = $configs['api'];
$query = '';
$apiKey = $configs['apiKey'];
$secret = $configs['secret'];
$canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
$signature = hash_hmac('sha1', $canonicalRequest, $secret);
 
$ch = curl_init();
//curl_setopt($ch, CURLOPT_URL, sprintf('%s%s%s', $api, $path, $query));
curl_setopt($ch, CURLOPT_URL, $api . $path);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $apiKey.':'.$signature);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Date: ' . gmdate('Ymd\THis\Z', $time)
]);

$httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
$response = curl_exec($ch);
$response = json_decode($response);
if( $response->status == 'error' ){
    echo 'Request Errors: <br>';
    foreach ($response->errors as $key => $error) {
    	foreach ($error as $v) {
    		echo $key . ': ' .$v . '<br>'; 	
    	} 
    }
}else{
	echo 'Record Deleted! <br><br>';
}
curl_close($ch);
?>

<a href="index.php">BACK TO LIST</a>