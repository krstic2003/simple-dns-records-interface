<?php

require 'config.php';

$time = time();
$method = 'GET';
$api = $configs['api'];
$query = '';
$apiKey = $configs['apiKey'];
$secret = $configs['secret'];
$canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
$signature = hash_hmac('sha1', $canonicalRequest, $secret);
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, sprintf('%s%s%s', $api, $path, $query));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $apiKey.':'.$signature);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Date: ' . gmdate('Ymd\THis\Z', $time),
]);
 
$response = curl_exec($ch);
curl_close($ch);
 
$response = json_decode($response);

?>