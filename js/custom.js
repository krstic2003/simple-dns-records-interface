function showHide() {
  	var x = document.getElementById("record-type").value;
  	if ( x == 'MX' || x == 'SRV' ) {
  		document.getElementById("prio").style.display = "inline-block";
  	}else{
  		document.getElementById("prio").style.display = "none"; 		
  	}

  	if ( x == 'SRV' ) {
  		document.getElementById("port").style.display = "inline-block";
  		document.getElementById("weight").style.display = "inline-block";
  	}else{
  		document.getElementById("port").style.display = "none";	
  		document.getElementById("weight").style.display = "none";	
  	}
}