<?php

// ini_set('display_errors', 1);
// ini_set('display_startup_errors', 1);
// error_reporting(E_ALL);

require 'config.php';


$type = $_POST['type'];
$name = $_POST['name'];
$content = $_POST['content'];
$prio = $_POST['prio'];
$port = $_POST['port'];
$weight = $_POST['weight'];
$ttl = $_POST['ttl'];
$note = $_POST['note'];

if ( $type == 'MX' ) {
	$query = [
	    'type' => $type,
	    'name' => $name,
	    'content'   => $content,
	    'ttl'   => $ttl,
	    'note'   => $note,
	    'prio' => $prio
	];
}elseif ( $type == 'SRV' ) {
	$query = [
	    'type' => $type,
	    'name' => $name,
	    'content'   => $content,
	    'ttl'   => $ttl,
	    'note'   => $note,
	    'prio' => $prio,
	    'port' => $port,
	    'weight' => $weight
	];
}else{
	$query = [
	    'type' => $type,
	    'name' => $name,
	    'content'   => $content,
	    'ttl'   => $ttl,
	    'note'   => $note
	];
}

$time = time();
$method = 'POST';
$path = '/v1/user/self/zone/'.$configs["domain"].'/record';
$api = $configs['api'];

$apiKey = $configs['apiKey'];
$secret = $configs['secret'];
$canonicalRequest = sprintf('%s %s %s', $method, $path, $time);
$signature = hash_hmac('sha1', $canonicalRequest, $secret);
 
$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $api . $path);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($query));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
curl_setopt($ch, CURLOPT_USERPWD, $apiKey.':'.$signature);
curl_setopt($ch, CURLOPT_HTTPHEADER, [
    'Date: ' . gmdate('Ymd\THis\Z', $time),
    'Content-Type: application/json'
]);
 
$response = curl_exec($ch);
$response = json_decode($response);
if( $response->status == 'error' ){
    echo 'Request Errors: <br>';
    foreach ($response->errors as $key => $error) {
    	foreach ($error as $v) {
    		echo $key . ': ' .$v . '<br>'; 	
    	} 
    }
}else{
	echo 'Record Added! <br><br>';
}
curl_close($ch);
?>

<a href="index.php">BACK TO LIST</a>